#ifndef BODY_HPP
#define BODY_HPP

#include <iostream>

SC_MODULE(Body) {
  SC_CTOR(Body) { std::cout << "Body constructor called" << std::endl; }

  ~Body() { std::cout << "Body destructor called" << std::endl; }
};

#endif
