#include <systemc.h>
#include <iostream>
#include "car.hpp"
#include "body.hpp"
#include "engine.hpp"

SC_HAS_PROCESS(Car);

Car::Car(sc_module_name name) : 
    sc_module(name), body(new Body("body")), engine(new Engine("engine")) {

  std::cout << "Car constructor called" << std::endl;
}

Car::~Car() {
  std::cout << "Car destructor called" << std::endl;

  delete body;
  delete engine; 
}
